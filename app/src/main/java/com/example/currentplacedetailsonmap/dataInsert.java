package com.example.currentplacedetailsonmap;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import static java.lang.Double.valueOf;

public class dataInsert extends MapsActivityCurrentPlace {
    EditText area, latitude, longitude;
    Button save;
    long maxid = 0;
    DatabaseReference reff;
    Member member;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.firebase_demo);
        area = (EditText)findViewById(R.id.name);
        latitude = (EditText)findViewById(R.id.phone);
        longitude = (EditText)findViewById(R.id.height);
        save = (Button)findViewById(R.id.save);
        member = new Member();

//        latitude_save = this.getSharedPreferences("latitudeKey" , Context.MODE_PRIVATE);
//        longitude_save = this.getSharedPreferences("longitudeKey" , Context.MODE_PRIVATE);

        reff = FirebaseDatabase.getInstance().getReference().child("Member");
        reff.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    maxid = (dataSnapshot.getChildrenCount());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                int txtAge = Integer.parseInt(age.getText().toString().trim());
                Float heightTxt = Float.parseFloat(longitude.getText().toString().trim());
                Float phoneTxt = Float.parseFloat(latitude.getText().toString().trim());
                member.setArea(area.getText().toString().trim());
                member.setLongitude(heightTxt);
                member.setLatitude(phoneTxt);
//                getDeviceLocation();
                Log.d("abcd", Double.toString(lat));

//                member.setLatitude(lat);
//                member.setLatitude(lng);
//                reff.child("member1").setValue(member);
                reff.child(String.valueOf(maxid + 1)).setValue(member);
//                reff.push().setValue(member);
                Toast.makeText(dataInsert.this, "Data Inserted", Toast.LENGTH_LONG).show();
            }
        });
    }
}
