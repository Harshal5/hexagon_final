package com.example.currentplacedetailsonmap;

import android.content.Intent;

public class Member {
    private String area;
    private Float latitude;
    private Float longitude;


    public String getName() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Float getPhone() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getHeight() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

}
